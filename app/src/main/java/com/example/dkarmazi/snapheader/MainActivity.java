package com.example.dkarmazi.snapheader;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyItemDecoration myItemDecoration = new MyItemDecoration(R.id.special_header_2, R.id.special_footer_2);
        MyAdapter myAdapter = new MyAdapter(MainActivity.this, getSampleData());

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.addItemDecoration(myItemDecoration);
        recyclerView.setAdapter(myAdapter);
    }


    private static List<String> getSampleData() {
        List<String> strings = new ArrayList<>();

        strings.add("zero");
        strings.add("one");
        strings.add("two");
        strings.add("three");
        strings.add("four");
        strings.add("five");
        strings.add("six");
        strings.add("seven");
        strings.add("eight");
        strings.add("nine");
        strings.add("ten");
        strings.add("eleven");
        strings.add("twelve");
        strings.add("thirteen");
        strings.add("fourteen");
        strings.add("fifteen");
        strings.add("sixteen");
        strings.add("seventeen");
        strings.add("eighteen");
        strings.add("nineteen");
        strings.add("twenty");

        return strings;
    }
}
