package com.example.dkarmazi.snapheader;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;


public class LocalHeaderView extends FrameLayout {
    private static final String TAG = "dkarmazi-dkarmazi";
    public static final String ARG_SUBTITLES_VISIBILITY = "arg_subtitles_visibility";
    public static final String ARG_SELECTED_ITEM_INDEX = "arg_selected_position";
    public static final String ARG_HORIZONTAL_SCROLL_OFFSET = "arg_horizontal_scroll_offset";

    public interface StateChangeListener {
        void onStateChanged(Bundle b);
    }

    private View titleWrapper;
    private RecyclerView subtitlesRecyclerView;
    private MySubtitlesAdapter mySubtitlesAdapter;
    private View menuView;
    private StateChangeListener stateChangeListener;

    public LocalHeaderView(Context context) {
        super(context);
        init();
    }

    public LocalHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LocalHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LocalHeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.item_header_local, this);

        titleWrapper = findViewById(R.id.special_header_title_wrapper);
        menuView = findViewById(R.id.special_header_1_menu);

        subtitlesRecyclerView = (RecyclerView) findViewById(R.id.special_header_subtitles);
        subtitlesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        mySubtitlesAdapter = new MySubtitlesAdapter(getContext().getApplicationContext(), getSubtitlesList());

        mySubtitlesAdapter.setPositionSelectListener(new MySubtitlesAdapter.PositionSelectListener() {
            @Override
            public void onPositionSelected() {
                // report selected item
                notifyStateChange();
            }
        });

        subtitlesRecyclerView.setAdapter(mySubtitlesAdapter);

        subtitlesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if(newState == RecyclerView.SCROLL_STATE_IDLE) {
                    // once the recycler view settles, report new offset
                    notifyStateChange();
                }
            }
        });

        setClickListeners();
    }


    private void setClickListeners() {
        titleWrapper.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSubTitleVisibility();
                notifyStateChange();
            }
        });

        menuView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = getPopupMenu(getContext(), v);
                menu.show();
            }
        });
    }


    private void toggleSubTitleVisibility() {
        if(subtitlesRecyclerView.getVisibility() == VISIBLE) {
            subtitlesRecyclerView.setVisibility(GONE);
        } else {
            subtitlesRecyclerView.setVisibility(VISIBLE);
        }
    }


    public Bundle saveViewInstanceState() {
        Bundle b = new Bundle();

        b.putBoolean(ARG_SUBTITLES_VISIBILITY, subtitlesRecyclerView.getVisibility() == VISIBLE);
        b.putInt(ARG_SELECTED_ITEM_INDEX, mySubtitlesAdapter.getSelectedPosition());
        b.putInt(ARG_HORIZONTAL_SCROLL_OFFSET, subtitlesRecyclerView.computeHorizontalScrollOffset());

        return b;
    }


    public void restoreViewInstanceState(Bundle b) {
        boolean isSubtitlesVisible = b.getBoolean(ARG_SUBTITLES_VISIBILITY);
        int selectedItemIndex = b.getInt(ARG_SELECTED_ITEM_INDEX);
        int horizontalScrollOffset = b.getInt(ARG_HORIZONTAL_SCROLL_OFFSET);

        mySubtitlesAdapter.setSelectedPosition(selectedItemIndex);
        subtitlesRecyclerView.setVisibility(isSubtitlesVisible ? VISIBLE : GONE);
        subtitlesRecyclerView.scrollToPosition(0);
        subtitlesRecyclerView.scrollBy(horizontalScrollOffset, 0);
    }


    public void setStateChangeListener(StateChangeListener stateChangeListener) {
        this.stateChangeListener = stateChangeListener;
    }

    private void notifyStateChange() {
        if(stateChangeListener != null) {
            stateChangeListener.onStateChanged(saveViewInstanceState());
        }
    }

    private static List<String> getSubtitlesList() {
        List<String> subtitlesList = new ArrayList<>();
        subtitlesList.add("Entry One");
        subtitlesList.add("Entry Two");
        subtitlesList.add("Entry Three");
        subtitlesList.add("Entry Four");
        subtitlesList.add("Entry Five");

        return subtitlesList;
    }

    public static PopupMenu getPopupMenu(Context context, View v) {
        PopupMenu menu = new PopupMenu(context, v) {
            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item){
                switch (item.getItemId()) {

                    case R.id.section_module_overflow_remove:
                        return true;

                    default:
                        return super.onMenuItemSelected(menu, item);

                }
            }
        };
        menu.inflate(R.menu.section_module_overflow_menu);

        return menu;
    }
}
