package com.example.dkarmazi.snapheader;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class MySubtitlesAdapter extends RecyclerView.Adapter {
    private static final String TAG = "dkarmazi-dkarmazi";

    public interface PositionSelectListener {
        void onPositionSelected();
    }

    private Context context;
    private List<String> dataList;
    private int selectedPosition = 0;
    private PositionSelectListener positionSelectListener;

    public MySubtitlesAdapter(Context context, List<String> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_header_local_subtitle, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MyViewHolder) holder).content.setText(dataList.get(position));
        ((MyViewHolder) holder).clickListener.setPosition(position);

        if(position == selectedPosition) {
            ((MyViewHolder) holder).content.setTextColor(Color.BLACK);
        } else {
            ((MyViewHolder) holder).content.setTextColor(Color.GRAY);
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void setPositionSelectListener(PositionSelectListener positionSelectListener) {
        this.positionSelectListener = positionSelectListener;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView content;
        MyClickListener clickListener;

        public MyViewHolder(View itemView) {
            super(itemView);

            content = (TextView) itemView.findViewById(R.id.special_header_subtitle);
            this.clickListener = new MyClickListener();

            itemView.setOnClickListener(clickListener);
        }
    }

    class MyClickListener implements View.OnClickListener {
        private int position;

        @Override
        public void onClick(View v) {
            Log.d(TAG, "subtitle: " + position);
            selectedPosition = position;
            notifyDataSetChanged();

            if(positionSelectListener != null) {
                positionSelectListener.onPositionSelected();
            }
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }
}
