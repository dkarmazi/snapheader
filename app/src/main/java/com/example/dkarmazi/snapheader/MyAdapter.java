package com.example.dkarmazi.snapheader;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class MyAdapter extends RecyclerView.Adapter {
    private static final String TAG = "dkarmazi-dkarmazi";
    private static final int TYPE_STANDARD = 1, TYPE_WITH_LOCAL_HEADER = 2, TYPE_WITH_NON_INTERACTIVE_HEADER = 3;
    private Context context;
    private List<String> dataList;


    public MyAdapter(Context context, List<String> dataList) {
        this.context = context;
        this.dataList = dataList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if(viewType == TYPE_WITH_LOCAL_HEADER) {
            view = LayoutInflater.from(context).inflate(R.layout.item_with_local_header, parent, false);
        } else if(viewType == TYPE_WITH_NON_INTERACTIVE_HEADER) {
            view = LayoutInflater.from(context).inflate(R.layout.item_with_non_interactive_header, parent, false);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.item_standard, parent, false);
        }

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(((MyViewHolder) holder).content != null) {
            ((MyViewHolder) holder).content.setText(position + ": " + dataList.get(position));
        }

        ((MyViewHolder) holder).clickListener.setLog("" + position);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 3) {
            return TYPE_WITH_LOCAL_HEADER;
        } else if(position == 4 || position == 7 || position == 8) {
            return TYPE_WITH_NON_INTERACTIVE_HEADER;
        } else {
            return TYPE_STANDARD;
        }
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView content;
        MyClickListener clickListener;

        public MyViewHolder(View itemView) {
            super(itemView);

            content = (TextView) itemView.findViewById(R.id.content);
            this.clickListener = new MyClickListener();

            View headerMenu = itemView.findViewById(R.id.special_header_1_menu);
            if(headerMenu != null) {
                headerMenu.setOnClickListener(clickListener);
            }
        }
    }

    class MyClickListener implements View.OnClickListener {
        private String log;

        @Override
        public void onClick(View v) {
            LocalHeaderView.getPopupMenu(context, v).show();

            Log.d(TAG, log);
        }

        public void setLog(String log) {
            this.log = log;
        }
    }
}
