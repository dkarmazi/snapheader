package com.example.dkarmazi.snapheader;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class MyItemDecoration extends RecyclerView.ItemDecoration {
    private final int headerViewId, footerViewId;

    public MyItemDecoration(int headerLayoutId, int footerLayoutId) {
        this.headerViewId = headerLayoutId;
        this.footerViewId = footerLayoutId;
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(c, parent, state);

            View child = parent.getChildAt(0);
            View header = child.findViewById(headerViewId);

            if(header != null) {
                if(child.getTop() < 0) {
                    // when the child top is off the screen
                    View footer = child.findViewById(footerViewId);

                    if(child.getBottom() > header.getHeight() + footer.getHeight()) {
                        // while there is space between the header and the footer
                        // draw impression of the header at the top of the canvas
                        header.draw(c);
                    } else {
                        // if there is no space left between header and footer
                        // the footer will push the header off the screen.
                        // In this case we have to translate canvas up on Y axis
                        // so that the header impression appears to be pushed.
                        c.translate(0, child.getBottom() - header.getHeight() - footer.getHeight());
                        header.draw(c);
                    }
                }
        }
    }
}
