package com.example.dkarmazi.snapheader;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

public class MyBehavior extends CoordinatorLayout.Behavior<LocalHeaderView> {
    private static final String TAG = "dkarmazi-dkarmazi";
    private boolean isHeaderSticky = false;
    private View recyclerChild = null;
    private LocalHeaderView headerInRecycler = null;
    private View footer = null;


    public MyBehavior() {}

    public MyBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, final LocalHeaderView child, final View dependency) {
        if(dependency instanceof RecyclerView) {
            // register recycler view scroll listener as we need to constantly monitor
            // position of special header inside the recycler view.
            // Build-in behavior only supports scroll, but fling does not report frequently
            ((RecyclerView) dependency).addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    handleRecyclerViewScrollOrFling(((RecyclerView) dependency), child);
                }
            });

            child.setStateChangeListener(new LocalHeaderView.StateChangeListener() {
                @Override
                public void onStateChanged(Bundle b) {
                    if(headerInRecycler != null) {
                        headerInRecycler.restoreViewInstanceState(b);
                    }
                }
            });

            return true;
        }

        return false;
    }


    private void handleRecyclerViewScrollOrFling(RecyclerView recyclerView, LocalHeaderView localHeaderView) {
        // we are only interested in the first child of the recycler view
        recyclerChild = recyclerView.getChildAt(0);
        headerInRecycler = (LocalHeaderView) recyclerChild.findViewById(R.id.special_header_1);

        if(headerInRecycler != null) {
            footer = recyclerChild.findViewById(R.id.special_footer_1);

            if(recyclerChild.getTop() < 0) {
                // CASE 1: child top is off the screen
                if(recyclerChild.getBottom() > headerInRecycler.getHeight() + footer.getHeight()) {
                    // CASE 1.1: sticky header: space between header and footer is available
                    if(!isHeaderSticky) {
                        isHeaderSticky = true;
                        localHeaderView.restoreViewInstanceState(headerInRecycler.saveViewInstanceState()); // activity header displays state of the header in recycler view
                        localHeaderView.setTranslationY(0f);
                        localHeaderView.setVisibility(View.VISIBLE);
                    }
                } else {
                    // CASE 1.2: non-sticky header: no space between header and footer
                    // the footer pushes the header off the screen.
                    if(isHeaderSticky) {
                        isHeaderSticky = false;
                    }
                    localHeaderView.setTranslationY(recyclerChild.getBottom() - headerInRecycler.getHeight() - footer.getHeight());
                }
            } else {
                // CASE 2: child top is on the screen
                if(isHeaderSticky) {
                    isHeaderSticky = false;
                    localHeaderView.setVisibility(View.GONE);
                }
            }
        } else {
            // CASE 3: recycler view item 0 does not have header.
            // hide activity header in case CASE 2 was skipped due to fling
            if(isHeaderSticky) {
                isHeaderSticky = false;
                localHeaderView.setVisibility(View.GONE);
            }
        }
    }
}